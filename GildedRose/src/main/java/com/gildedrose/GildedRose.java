package com.gildedrose;

import java.util.Arrays;

public class GildedRose {

    public static final String BRIE = "Aged Brie";
    public static final String PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public static final String CONJURED = "Conjured Mana Cake";

    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        if(items != null) {
            Arrays.stream(items).parallel().forEach(item -> updateItem(item));
        }
    }

    public static void updateItem(Item item) {
        switch(item.name) {
            case BRIE:
                if(item.quality < 50) {
                    item.quality += 1;
                }
                item.sellIn -= 1;
                if(item.sellIn < 0 && item.quality < 50) {
                    item.quality += 1;
                }
                break;
            case PASSES:
                if(item.quality < 50) {
                    item.quality += 1;
                    if(item.sellIn < 11 && item.quality < 50) {
                        item.quality += 1;
                    }
                    if(item.sellIn < 6 && item.quality < 50) {
                        item.quality += 1;
                    }
                }
                item.sellIn -= 1;
                if(item.sellIn < 0) {
                    item.quality = 0;
                }
                break;
            case SULFURAS://do nothing
                break;
            case CONJURED:
                if(item.quality > 0) {
                    item.quality -= item.quality > 1 ? 2 : 1;
                }
                item.sellIn -= 1;
                if(item.sellIn < 0 && item.quality > 0) {
                    item.quality -= item.quality > 1 ? 2 : 1;
                }
                break;
            default:
                if(item.quality > 0) {
                    item.quality -= 1;
                }
                item.sellIn -= 1;
                if(item.sellIn < 0 && item.quality > 0) {
                    item.quality -= 1;
                }
                break;
        }
    }

}