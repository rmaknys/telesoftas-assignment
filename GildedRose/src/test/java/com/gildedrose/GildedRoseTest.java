package com.gildedrose;

import static com.gildedrose.GildedRose.*;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GildedRoseTest {

    private static final String REGULAR = "+5 Dexterity Vest";

    private GildedRose app = new GildedRose(null);

    @Test
    public void testItemsIsNull_shouldNeverFail() {
        app.updateQuality();
    }

    @Test
    public void testRegularItem() {
        //given
        Item item1 = new Item(REGULAR, 10, 20);
        Item item2 = new Item(REGULAR, 1, 20);
        Item item3 = new Item(REGULAR, 0, 20);
        Item item4 = new Item(REGULAR, 5, 1);
        Item item5 = new Item(REGULAR, 5, 0);
        Item item6 = new Item(REGULAR, -1, 1);
        app.items = new Item[] {item1, item2, item3, item4, item5, item6};

        //when
        app.updateQuality();

        //then
        //standardCase
        assertEquals("sellIn value incorrect", 9, item1.sellIn);
        assertEquals("quality value incorrect", 19, item1.quality);
        //sellInToZero
        assertEquals("sellIn value incorrect", 0, item2.sellIn);
        assertEquals("quality value incorrect", 19, item2.quality);
        //sellInToNegative
        assertEquals("sellIn value incorrect", -1, item3.sellIn);
        assertEquals("quality value incorrect", 18, item3.quality);
        //qualityToZero
        assertEquals("sellIn value incorrect", 4, item4.sellIn);
        assertEquals("quality value incorrect", 0, item4.quality);
        //qualityNeverNegative
        assertEquals("sellIn value incorrect", 4, item5.sellIn);
        assertEquals("quality value incorrect", 0, item5.quality);
        //sellInToNegativeQualityNeverNegative
        assertEquals("sellIn value incorrect", -2, item6.sellIn);
        assertEquals("quality value incorrect", 0, item6.quality);
    }

    @Test
    public void testBrieItem() {
        //given
        Item item1 = new Item(BRIE, 10, 20);
        Item item2 = new Item(BRIE, 1, 20);
        Item item3 = new Item(BRIE, 0, 20);
        Item item4 = new Item(BRIE, 5, 49);
        Item item5 = new Item(BRIE, 5, 50);
        Item item6 = new Item(BRIE, -1, 49);
        app.items = new Item[] {item1, item2, item3, item4, item5, item6};

        //when
        app.updateQuality();

        //then
        //standardCase
        assertEquals("sellIn value incorrect", 9, item1.sellIn);
        assertEquals("quality value incorrect", 21, item1.quality);
        //sellInToZero
        assertEquals("sellIn value incorrect", 0, item2.sellIn);
        assertEquals("quality value incorrect", 21, item2.quality);
        //sellInToNegative
        assertEquals("sellIn value incorrect", -1, item3.sellIn);
        assertEquals("quality value incorrect", 22, item3.quality);
        //qualityToMax
        assertEquals("sellIn value incorrect", 4, item4.sellIn);
        assertEquals("quality value incorrect", 50, item4.quality);
        //qualityNeverMoreThan50
        assertEquals("sellIn value incorrect", 4, item5.sellIn);
        assertEquals("quality value incorrect", 50, item5.quality);
        //sellInToNegativeQualityNeverMoreThan50
        assertEquals("sellIn value incorrect", -2, item6.sellIn);
        assertEquals("quality value incorrect", 50, item6.quality);
    }

    @Test
    public void testPassesItem() {
        //given
        Item item1 = new Item(PASSES, 15, 20);
        Item item2 = new Item(PASSES, 10, 20);
        Item item3 = new Item(PASSES, 5, 20);
        Item item4 = new Item(PASSES, 1, 20);
        Item item5 = new Item(PASSES, 0, 49);
        Item item6 = new Item(PASSES, 15, 49);
        Item item7 = new Item(PASSES, 15, 50);
        Item item8 = new Item(PASSES, 10, 49);
        Item item9 = new Item(PASSES, 5, 49);
        app.items = new Item[] {item1, item2, item3, item4, item5, item6, item7, item8, item9};

        //when
        app.updateQuality();

        //then
        //standardCase
        assertEquals("sellIn value incorrect", 14, item1.sellIn);
        assertEquals("quality value incorrect", 21, item1.quality);
        //qualityUpBy2
        assertEquals("sellIn value incorrect", 9, item2.sellIn);
        assertEquals("quality value incorrect", 22, item2.quality);
        //qualityUpBy3
        assertEquals("sellIn value incorrect", 4, item3.sellIn);
        assertEquals("quality value incorrect", 23, item3.quality);
        //sellInToZero
        assertEquals("sellIn value incorrect", 0, item4.sellIn);
        assertEquals("quality value incorrect", 23, item4.quality);
        //sellInToNegative
        assertEquals("sellIn value incorrect", -1, item5.sellIn);
        assertEquals("quality value incorrect", 0, item5.quality);
        //qualityToMax
        assertEquals("sellIn value incorrect", 14, item6.sellIn);
        assertEquals("quality value incorrect", 50, item6.quality);
        //qualityNeverMoreThan50
        assertEquals("sellIn value incorrect", 14, item7.sellIn);
        assertEquals("quality value incorrect", 50, item7.quality);
        //qualityUpBy2ButNeverMoreThan50
        assertEquals("sellIn value incorrect", 9, item8.sellIn);
        assertEquals("quality value incorrect", 50, item8.quality);
        //qualityUpBy3ButNeverMoreThan50
        assertEquals("sellIn value incorrect", 4, item9.sellIn);
        assertEquals("quality value incorrect", 50, item9.quality);
    }

    @Test
    public void testSulfurasItem() {
        //given
        Item item1 = new Item(SULFURAS, 5, 80);
        Item item2 = new Item(SULFURAS, 0, 80);
        Item item3 = new Item(SULFURAS, -5, 80);
        app.items = new Item[] {item1, item2, item3};

        //when
        app.updateQuality();

        //then
        //sellInPositiveQualitySame
        assertEquals("sellIn value incorrect", 5, item1.sellIn);
        assertEquals("quality value incorrect", 80, item1.quality);
        //sellInZeroQualitySame
        assertEquals("sellIn value incorrect", 0, item2.sellIn);
        assertEquals("quality value incorrect", 80, item2.quality);
        //sellInNegativeQualitySame
        assertEquals("sellIn value incorrect", -5, item3.sellIn);
        assertEquals("quality value incorrect", 80, item3.quality);
    }

    @Test
    public void testConjuredItem() {
        //given
        Item item1 = new Item(CONJURED, 10, 20);
        Item item2 = new Item(CONJURED, 1, 20);
        Item item3 = new Item(CONJURED, 0, 20);
        Item item4 = new Item(CONJURED, 5, 2);
        Item item5 = new Item(CONJURED, 5, 1);
        Item item6 = new Item(CONJURED, -1, 1);
        app.items = new Item[] {item1, item2, item3, item4, item5, item6};

        //when
        app.updateQuality();

        //then
        //standardCase
        assertEquals("sellIn value incorrect", 9, item1.sellIn);
        assertEquals("quality value incorrect", 18, item1.quality);
        //sellInToZero
        assertEquals("sellIn value incorrect", 0, item2.sellIn);
        assertEquals("quality value incorrect", 18, item2.quality);
        //sellInToNegative
        assertEquals("sellIn value incorrect", -1, item3.sellIn);
        assertEquals("quality value incorrect", 16, item3.quality);
        //qualityToZero
        assertEquals("sellIn value incorrect", 4, item4.sellIn);
        assertEquals("quality value incorrect", 0, item4.quality);
        //qualityNeverNegative
        assertEquals("sellIn value incorrect", 4, item5.sellIn);
        assertEquals("quality value incorrect", 0, item5.quality);
        //sellInToNegativeQualityNeverNegative
        assertEquals("sellIn value incorrect", -2, item6.sellIn);
        assertEquals("quality value incorrect", 0, item6.quality);
    }

}
