package lt.cukrus.service;

import com.gildedrose.GildedRose;
import com.gildedrose.Item;
import lt.cukrus.domain.ShopItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TaskScheduler {

    @Autowired
    private ItemRepository repo;

    @Scheduled(cron = "0 0 0 * * *")
    public void updateQuality() {
        List<ShopItem> shopItems = repo.findAll();
        shopItems.parallelStream().forEach(shopItem -> {
            Item item = new Item(shopItem.getName(), shopItem.getSellIn(), shopItem.getQuality());
            GildedRose.updateItem(item);
            shopItem.updateFromItem(item);
            repo.save(shopItem);
        });
    }

}
