package lt.cukrus.domain;

import com.gildedrose.Item;
import org.springframework.data.annotation.Id;

public class ShopItem {

    @Id
    private String id;
    private String name;
    private int sellIn;
    private int quality;

    public void updateFromItem(Item item) {
        this.name = item.name;
        this.sellIn = item.sellIn;
        this.quality = item.quality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSellIn() {
        return sellIn;
    }

    public void setSellIn(int sellIn) {
        this.sellIn = sellIn;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }
}
