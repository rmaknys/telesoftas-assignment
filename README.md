# TeleSoftas-assignment

Assignment for TeleSoftas

# NOTES

Refactoring and all other assignment parts were done abiding by GildedRose requirements,
if the requirements preventing Item class modification vere lifted,
the assignment would have been done in a more object oriented manner.
Assignment done using MongoDB default spring boot config.
Items exposed via REST using default spring boot REST config.